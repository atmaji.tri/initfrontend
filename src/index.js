import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import api from 'utils/api';
import AppContainer from 'features/app';

api.setupInterceptors(store());

ReactDOM.render(
    <Provider store={store()}>
        <AppContainer />
    </Provider>,
    document.getElementById('root')
);

